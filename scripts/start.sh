#!/bin/bash

set -e

/var/www/project/setup_gunicorn.sh
source /var/www/project/ve/bin/activate
/var/www/project/$APPLICATION_NAME/gunicorn.sh
